'use strict'
require('../tests')

let app         = require('../../app')
let supertest   = require('co-supertest')
let sinon       = require('sinon')

let User          = require('../../models/user')
let KafkaProducer = require('../../services/kafka-producer')

function request() {
  return supertest(app.listen())
}

describe('Routes', function() {
  beforeEach(function() {
    // This doesn't seem to work :(
    // sinon.stub(KafkaProducer.protoype, 'send')
  })

  afterEach(function* () {
    yield User.destroy({truncate: true})
  })

  it('POST /log', function* (){
    let result = yield request().post('/log').send({actionId: "STUFF_HAPPENED"}).end();
    result.status.should.equal(200)
  })

  it('POST /classes/user', function* (){
    let result = yield request().post('/classes/user').send({name: "hi", email: "weird@al.com"}).end();
    result.status.should.equal(201)
  })

  it('PUT /classes/user/id ', function*(){

    // Unknown ID
    let result = yield request().put('/classes/user/0000').send({name: "hi", email: "weird@al.com"}).end();
    result.status.should.equal(404)

    yield User.create({name: "Lou Ferrigno", email: "lou@ferrigno.com"})
    let user = yield User.findOne()
    let otherResult = yield request().put('/classes/user/'+user.id).send({name: "hi", email: "weird@al.com"}).end();
    otherResult.status.should.equal(200)

    yield user.reload()
    user.name.should.equal("hi")
    //email shouldn't be updated
    user.email.should.equal("lou@ferrigno.com")
  })
})
