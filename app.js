'use strict';
let koa      = require('koa')
let app      =  koa();
let router   = require('koa-router')()
let koaBody  = require('koa-body')()
let params   = require('params')

require('dotenv').load()
let KafkaProducer = require('./services/kafka-producer')
let User          = require('./models/user')
let Log           = require('./models/log')

router
  .get('/', function*(){
    this.body   = 'Hello World'
    this.status = 200
  })
  .post('/log', koaBody, function* (next){
    let logParams = params(this.request.body).only('actionId','userId', 'data')
    let log = new Log(logParams)
    KafkaProducer.send('log', log.toString())
    this.status = 200
  })
  .post('/classes/user', koaBody, function* (next){
    let userParams = params(this.request.body).only('name', 'email', 'password')
    try {
      yield User.create(userParams)
      this.status = 201

      let log = new Log({actionId: "USER_SIGNUP", data: userParams})
      KafkaProducer.send('log', log.toString())
    } catch(SequelizeValidationError) {
      this.status = 422
    }
  })
  .put('/classes/user/:id', koaBody, function* (next){
    let userParams = params(this.request.body).only('name', 'password')
    let userId     = parseInt(this.params.id)
    let user       = yield User.findById(userId)
    try {
      if (user) {
        yield user.updateAttributes(userParams)
        this.status = 200

        let log = new Log({actionId: "USER_EDIT_PROFILE", userId: userId, data: userParams})
        KafkaProducer.send('log', log.toString())
      } else {
        this.status = 404
      }
    } catch(SequelizeValidationError) {
      this.status = 422
    }
  })

app
  .use(router.routes())
  .use(router.allowedMethods());

if(!module.parent) {
  app.listen(3000);
}

module.exports = app
