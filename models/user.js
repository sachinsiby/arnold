'use strict';
let Sequelize =  require('sequelize');
let sequelize =  require("./database.js");

let User = sequelize.define('user', {
  name: { type: Sequelize.STRING },
  email: {
    type: Sequelize.STRING,
    validate: {
      isEmail: true
    }
  },
  password: { type: Sequelize.STRING }
})
User.sync()

module.exports = User


//curl -H "Content-Type: application/json" -X POST -d '{"name": "sachin","password":"xyz", "email": "sachin@gmail.com"}' http://localhost:3000/classes/user
