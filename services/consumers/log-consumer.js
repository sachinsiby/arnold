'use strict';
let kafka = require('kafka-node')
let Consumer = kafka.Consumer
let client = new kafka.Client()
let consumer = new Consumer(
      client,
      [
        { topic: 'log'}
      ],
      {
        autoCommit: false
      }
    );

let fs = require('fs');

consumer.on('message', function(message){
  fs.appendFile("app.log", message.value)
});
