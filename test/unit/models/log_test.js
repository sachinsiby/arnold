'use strict'
require('../../tests')
let Log = require('../../../models/log')

describe('Log', function() {
  describe('#toString()', function () {
    it ('returns stringified attributes', function() {
      let log          = new Log({actionId: 'USER_SIGNUP', userId: 1, data: {this: 'body'}})
      let result       = log.toString()
      let resultObj    = JSON.parse(result)
      result.should.be.a.String()
      resultObj.should.have.property('actionId')
      resultObj.should.have.property('userId')
      resultObj.should.have.property('data')
    })
  })
})
