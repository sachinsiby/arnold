'use strict';
let kafka = require('kafka-node'),
    Producer = kafka.Producer,
    client = new kafka.Client(),
    producer = new Producer(client);

class KafkaProducer {
  static send(topic, message) {
    let payload = {
      topic: topic,
      messages: message
    }
    //TODO: Sinon stubbing and es6 are weird so this workaround
    if (process.env.NODE_ENV != 'test') {
      producer.send([payload], () => {})
    }
  }
}

module.exports = KafkaProducer
