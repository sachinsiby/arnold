'use strict'
require('../../tests')

let User = require('../../../models/user')
describe('User', function() {
  afterEach(function* () {
    yield User.destroy({truncate: true})
  })

  context('validations', function () {
    it ('validates email format', function* () {
      try {
        yield User.create({email: "invalid_email", name: "HI", password: "xxx"})
        /**
          For some reason .should.raise is not working the way I want it to
          with generators so this is unfortunately a bit awkward
        **/
        throw new Error("fail");
      } catch(SequelizeValidationError) {
        // yay!
      }
    })
  }),

  context('persistence', function() {
    it('creates', function* () {
      let firstCount = yield User.count()
      firstCount.should.equal(0)
      yield User.create({email: "a@w.com", name: "HI", password: "xxx"})
      let finalCount = yield User.count()
      finalCount.should.equal(1)
    })
    it('updates', function* () {
      yield User.create({email: "a@w.com", name: "Arnold", password: "xxx"})
      let user = yield User.findOne()
      user.updateAttributes({name: "Ronnie"})

      let finalCount = yield User.count()
      finalCount.should.equal(1)

      let lastUser = yield User.findOne()
      lastUser.name.should.equal('Ronnie')
    })
  })
})
