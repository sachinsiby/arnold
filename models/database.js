'use strict'
let Sequelize = require('sequelize');
const connectionString = process.env.DATABASE_URL;
let sequelize = new Sequelize(connectionString, {logging: process.env.NODE_ENV === 'development'});

module.exports = sequelize
