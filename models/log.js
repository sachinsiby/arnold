'use strict';
class Log {
  constructor(options) {
    this.actionId = options.actionId
    this.userId   = options.userId
    this.data     = options.data
  }

  toString() {
    return JSON.stringify({
      actionId: this.actionId,
      userId: this.userId,
      data: this.data
    })
  }
}

module.exports = Log
